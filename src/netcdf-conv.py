from datetime import datetime
import h5py
import numpy as np
import cfdm


with h5py.File('/tmp/uva-test-current.h5', mode='r') as f:
    hobo_data = f['data'][...]
    sensors = f['sensors'][...]
    measurement_types = f['/measurement_types'].attrs['values']
    data_type_id = f['/data_type_id'].attrs['values']
    loggers = f['loggers'][...]

# No duplicates...
hobo_data = np.unique(hobo_data)

# Keep only sensors that have logger metadata...
sensors = sensors[np.isin(sensors['sn_logger'], loggers['sn'])]

# Keep only data for which there are sensor metadata...
hobo_data = hobo_data[np.isin(hobo_data['sensor_sn'], sensors['sn'])]

# Sort leftover data on time...
hobo_data.sort(order='timestamp')

# Sensor IDs as strings...
sensor_ids = np.unique(sensors['sn']).astype(str)

# Available standard names...
std_names = np.unique(sensors['standard_name'])

# What goes into the output file...
hobo_content = list()

date_created = datetime.utcnow().isoformat() + '+00'

# Loop over sensors of the same standard_name...
for sname in std_names:
    if sname == b'LiPo Battery':
        print(f'Skipping sensors with standard name "{sname}"')
        continue
    else:
        print(f'Processing sensors with standard name "{sname}"')

    grp_name = sname.decode('utf-8')
    fld_name = grp_name

    # Collect data for the same standard name...
    sn_sensors = sensors[sensors['standard_name'] == sname]
    sn_hobo_data = hobo_data[np.isin(hobo_data['sensor_sn'], sn_sensors['sn'])]
    val_min = np.amin(sn_hobo_data['si_value'])
    val_max = np.amax(sn_hobo_data['si_value'])
    tstamps = np.unique(sn_hobo_data['timestamp'])

    sname = sname.decode('utf-8')

    # CF domain axes...
    sensor_dim = cfdm.DomainAxis(len(sn_sensors))
    sensor_dim.nc_set_dimension('sensor')
    sensor_dim.nc_set_dimension_groups([grp_name])

    time_dim = cfdm.DomainAxis(tstamps.size)
    time_dim.nc_set_dimension('time')
    time_dim.nc_set_dimension_groups([grp_name])

    # CF dimension coordinates...
    time = cfdm.DimensionCoordinate()
    time.set_data(tstamps)
    time.set_properties(
        {
            'standard_name': 'time',
            'calendar': 'gregorian',
            'long_name': 'observation time',
            'units': 'seconds since 1970-01-01T00:00:00+00',
        }
    )
    time.nc_set_variable('time')
    time.nc_set_variable_groups([grp_name])

    # CF auxiliary coordinates...
    sensor_id = cfdm.AuxiliaryCoordinate()
    sensor_id.set_data(sn_sensors['sn'].astype(str))
    sensor_id.set_properties(
        {
            'long_name': 'sensor id',
            'cf_role': 'timeseries_id',
        }
    )
    sensor_id.nc_set_variable('sensor_id')
    sensor_id.nc_set_variable_groups([grp_name])

    station_en = cfdm.AuxiliaryCoordinate()
    station_en.set_data(
        [loggers[loggers['sn'] == sensors[sensors['sn'] == _]['sn_logger']]['name'].item().decode('utf-8')
         for _ in sn_sensors['sn']])
    station_en.set_properties({'long_name': 'Station name (eng)', })
    station_en.nc_set_variable('station_en')
    station_en.nc_set_variable_groups([grp_name])

    station_ipk = cfdm.AuxiliaryCoordinate()
    station_ipk.set_data(
        [loggers[loggers['sn'] == sensors[sensors['sn'] == _]['sn_logger']]['name_ipk'].item().decode('utf-8')
         for _ in sn_sensors['sn']])
    station_ipk.set_properties({'long_name': 'Station name (native)', })
    station_ipk.nc_set_variable('station_ipk')
    station_ipk.nc_set_variable_groups([grp_name])

    lat = cfdm.AuxiliaryCoordinate()
    lat.set_data([sensors[sensors['sn'] == _]['lat'].item() for _ in sn_sensors['sn']])
    lat.set_properties(
        {
            'long_name': 'sensor latitude',
            'standard_name': 'latitude',
            'units': 'degrees_north',
        }
    )
    lat.nc_set_variable('latitude')
    lat.nc_set_variable_groups([grp_name])

    lon = cfdm.AuxiliaryCoordinate()
    lon.set_data([sensors[sensors['sn'] == _]['lon'].item() for _ in sn_sensors['sn']])
    lon.set_properties(
        {
            'long_name': 'sensor longitude',
            'standard_name': 'longitude',
            'units': 'degrees_east',
        }
    )
    lon.nc_set_variable('longitude')
    lon.nc_set_variable_groups([grp_name])

    # Prepare actual sensor data...
    fill_val = np.unique(sn_sensors['missing_values'])
    if fill_val.size > 1:
        raise ValueError(f'{fill_val}: Multiple fill values')
    units = np.unique(sn_sensors['measurement_units'])
    if units.size > 1:
        units = f'ValueError({units}: Multiple measurement units)'
    else:
        units = units.item().decode('utf-8')
    long_name = np.unique(sn_sensors['long_name'])
    if long_name.size > 1:
        long_name = f'ValueError({long_name}: Multiple long names)'
    else:
        long_name = long_name.item().decode('utf-8')
    meas_data = np.full(
        (time_dim.get_size(), sensor_dim.get_size()), fill_val, dtype=np.float64)
    for i, s in enumerate(sn_sensors['sn']):
        sens_time = sn_hobo_data[sn_hobo_data['sensor_sn'] == s]['timestamp']
        sens_data = sn_hobo_data[sn_hobo_data['sensor_sn'] == s]['si_value']
        mask_where = np.isin(tstamps, sens_time)
        meas_data[mask_where, i] = sens_data

    data = cfdm.Data(meas_data, fill_value=fill_val, units=units)
    data.nc_set_hdf5_chunksizes(
        (min(30_000, time_dim.get_size()), min(2, sensor_dim.get_size())))

    # CF fields...
    geoparam = cfdm.Field()
    sensor_ax = geoparam.set_construct(sensor_dim)
    time_ax = geoparam.set_construct(time_dim)
    geoparam.set_construct(time, axes=time_ax)
    geoparam.set_construct(sensor_id, axes=sensor_ax)
    geoparam.set_construct(lat, axes=sensor_ax)
    geoparam.set_construct(lon, axes=sensor_ax)
    geoparam.set_construct(station_en, axes=sensor_ax)
    geoparam.set_construct(station_ipk, axes=sensor_ax)
    geoparam.set_data(data, axes=[time_ax, sensor_ax])
    geoparam.set_properties(
        {
            'standard_name': sname,
            'long_name': long_name,
            '_FillValue': data.get_fill_value(),
            'valid_min': val_min,
            'valid_max': val_max
        }
    )
    geoparam.apply_masking(inplace=True)
    geoparam.nc_set_variable(fld_name)
    geoparam.nc_set_variable_groups([grp_name])

    # cfdm.write(geoparam,
    #            f'hobo_cf_sample_{sname}.nc', fmt='NETCDF4', mode='w',
    #            file_descriptors={
    #                'featureType': 'timeseries',
    #                'date_created': date_created,
    #            })

    hobo_content.append(geoparam)

# Create the file and write out all the data...
cfdm.write(hobo_content,
           'hobo_cf_sample_all.nc', fmt='NETCDF4', mode='w',
           file_descriptors={
               'featureType': 'timeseries',
               'date_created': date_created,
            })

# Create a flattend file version...
cfdm.write(hobo_content,
           'hobo_cf_sample_all_nogroups.nc',
           fmt='NETCDF4', mode='w', group=False,
           file_descriptors={
               'featureType': 'timeseries',
               'date_created': date_created,
           })
